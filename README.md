# Fill In

Fill in is a simple solution which takes an input document (Scribus), replaces content and exports a PDF.

## Usage

Please read Installation section to set up your system. When that is done, the following script will open a Scribus document, assign a Python script and pass arguments to it.

```
scribus -g -ns -py fill-in.py template.sla
```

- `-g` means that Scribus will open in no GUI mode
- `-ns` stands for no splash
- `-py` expects path to Python script and arguments for it

## Installation

Scribus requires display to run. We can fake that. Install Xvfb.

```
sudo yum install xvfb
```

Install Scribus itself.

```
sudo yum install scribus
```

Use `xvfb-run` to open and close display without starting a service.

```
xvfb-run scribus -v
```

## License

Copyright Krisjanis Rijnieks 2020 and beyond
