from flask import Flask
from flask import render_template
from flask import request
from flask import send_file
import subprocess

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template("index.html")

@app.route('/agreement/prepare', methods=['POST'])
def prepare_agreement():
    if request.method == 'POST':
        ret = ''
        ret = ret + request.form['company_name'] + ', '
        ret = ret + request.form['company_business_id'] + ', '
        ret = ret + request.form['company_address'] + ', '
        ret = ret + request.form['company_contact_person'] + ', '
        ret = ret + request.form['company_invoicing_address']

        cmd = ['/usr/bin/xvfb-run', '/usr/bin/scribus', '-g', '-ns', '-py', '../test.py']

        cmd.append('--template')
        cmd.append('../document.sla')

        cmd.append('--company-name')
        cmd.append(request.form['company_name'])

        cmd.append('--company-business-id')
        cmd.append(request.form['company_business_id'])

        cmd.append('--company-address')
        cmd.append(request.form['company_address'])

        cmd.append('--company-contact-person')
        cmd.append(request.form['company_contact_person'])

        cmd.append('--company-invoicing-address')
        cmd.append(request.form['company_invoicing_address'])

        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        except EnvironmentError:
            return 'Error'
        stdout = p.communicate()[0]
        return 'Success'
    return render_template("index.html")

@app.route('/agreement/download')
def download_agreement():
    return send_file('../document.pdf', as_attachment=True)
