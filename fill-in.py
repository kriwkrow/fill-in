import os
import scribus

print(sys.argv)

if len(sys.argv) < 2:
    print('No input file')

input_file = sys.argv[1]
print(input_file)
scribus.openDoc(input_file)

if scribus.haveDoc():
    filename = os.path.splitext(scribus.getDocName())[0]
    print(dir(scribus))
    scribus.setText('How about this', 'Text4')
    for i in range(1, scribus.pageCount() + 1):
        print('Going to page ' + str(i))
        scribus.gotoPage(i)
        objects = scribus.getAllObjects()
        print(objects)

    pdf = scribus.PDFfile()
    pdf.file = filename + ".pdf"
    pdf.save()
else :
    print("No file open")
